<?php

namespace app\controllers;

use Yii;
use app\models\User_2;
use app\models\User_2Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * User_2Controller implements the CRUD actions for User_2 model.
 */
class User_2Controller extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create','update','delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create','update','delete'],
                        'roles' => ['manageUsers'],
                    ],
                ],
            ],
        ];            
    }


    /**
     * Lists all User_2 models.
     * @return mixed
     */
    public function actionIndex()
    {
        //if (\Yii::$app->user->can('viewUser')) {
        $searchModel = new User_2Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
       // }
       // throw new ForbiddenHttpException('Sorry you are not allowed to view users');
    }

    /**
     * Displays a single User_2 model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
       // if (\Yii::$app->user->can('viewUser')) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
  //  }
   // throw new ForbiddenHttpException('Sorry you are not allowed to view user');
    }

    /**
     * Creates a new User_2 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      //  if (\Yii::$app->user->can('manageUsers')) {
        $model = new User_2();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
   // }
   // throw new ForbiddenHttpException('Sorry you are not allowed to create users');    
    }

    /**
     * Updates an existing User_2 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      //  if (\Yii::$app->user->can('manageUsers')) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
       // }
       // throw new ForbiddenHttpException('Sorry you are not allowed to update users');
    }

    /**
     * Deletes an existing User_2 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {   
       // if (\Yii::$app->user->can('manageUsers')) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
   // }
   // throw new ForbiddenHttpException('Sorry you are not allowed to delete users');
    }

    /**
     * Finds the User_2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User_2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User_2::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
