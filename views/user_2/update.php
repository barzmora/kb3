<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User_2 */

$this->title = 'Update User 2: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'User 2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-2-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
